<%--
  Created by IntelliJ IDEA.
  User: Peter Shumway
  Date: 7/6/2020
  Time: 9:06 PM
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>MadLib Server</title>
  </head>
  <body>
  <h2>The server is up and running!</h2>
  <br>
  <p>To try out Pete's MadLibs click <a href="${pageContext.request.contextPath}/landing.html">here</a>.</p>
  </body>
</html>
