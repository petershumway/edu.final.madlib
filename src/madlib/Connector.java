///////////////////////////////////////////////////////////
//Connector.java
// A simple class to connect the Tomcat server to the
// MySQL database.
///////////////////////////////////////////////////////////
package madlib;

import javax.persistence.*;

// this connects the class to the entity in the database
@Entity
@Table(name = "story")

public class Connector {

    // This class simply connects the story ID and the JSON that houses the story object to the columns in the
    // MySQL database.
    @Id
    @Column(name = "idstory")
    private int storyID;

    @Column(name = "story")
    private String JSONStory;

    // Getters and Setters
    public int getStoryID() {
        return storyID;
    }

    public void setStoryID(int storyID) {
        this.storyID = storyID;
    }

    public String getJSONStory() {
        return JSONStory;
    }

    public void setJSONStory(String JSONStory) {
        this.JSONStory = JSONStory;
    }

}
