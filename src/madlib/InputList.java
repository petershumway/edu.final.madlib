///////////////////////////////////////////////////////////
//InputList.java
// This class is made to dynamically manage all the user input
// that is collected in the form on the site.
///////////////////////////////////////////////////////////

package madlib;

import java.util.*;

public class InputList {

    // Data member variables
    private ArrayList nounArrayList;
    private ArrayList pNounArrayList;
    private ArrayList adjectiveArrayList;
    private ArrayList verbArrayList;
    private ArrayList verbIngArrayList;
    private ArrayList bPartArrayList;
    private ArrayList numArrayList;
    private ArrayList ptVerbArrayList;
    private ArrayList verbSArrayList;
    private ArrayList placeArrayList;

    // Constructor
    InputList(){
        this.nounArrayList = new ArrayList();
        this.pNounArrayList = new ArrayList();
        this.adjectiveArrayList = new ArrayList();
        this.verbArrayList = new ArrayList();
        this.verbIngArrayList = new ArrayList();
        this.bPartArrayList = new ArrayList();
        this.numArrayList = new ArrayList();
        this.ptVerbArrayList = new ArrayList();
        this.verbSArrayList = new ArrayList();
        this.placeArrayList = new ArrayList();
    }

    // Getters and Setters

    //Noun
    public void setNoun(String newNoun) {
        this.nounArrayList.add(newNoun);
    }
    public String getNoun(int position) {
        return (String)this.nounArrayList.get(position);
    }

    //Plural Noun
    public void setPNoun(String newPNoun) {
        this.pNounArrayList.add(newPNoun);
    }
    public String getPNoun(int position) {
        return (String)this.pNounArrayList.get(position);
    }

    //Adjective
    public void setAdjective(String newAdjective) {
        this.adjectiveArrayList.add(newAdjective);
    }
    public String getAdjective(int position) {
        return (String)this.adjectiveArrayList.get(position);
    }

    //Verb
    public void setVerb(String newVerb) {
        this.verbArrayList.add(newVerb);
    }
    public String getVerb(int position) {
        return (String)this.verbArrayList.get(position);
    }

    //Verb ending in ING
    public void setVerbIng(String newVerbIng) {
        this.verbIngArrayList.add(newVerbIng);
    }
    public String getVerbIng(int position) {
        return (String)this.verbIngArrayList.get(position);
    }

    //Body Part
    public void setBPart(String newBPart) {
        this.bPartArrayList.add(newBPart);
    }
    public String getBPart(int position) {
        return (String)this.bPartArrayList.get(position);
    }

    //Number
    public void setNumber(String newNumber) {
        this.numArrayList.add(newNumber);
    }
    public String getNumber(int position) {
        return (String)this.numArrayList.get(position);
    }

    //Past Tense Verb
    public void setPtVerb(String newPtVerb) {
        this.ptVerbArrayList.add(newPtVerb);
    }
    public String getPtVerb(int position) {
        return (String)this.ptVerbArrayList.get(position);
    }

    //Verb ending in S
    public void setVerbS(String newVerbS) {
        this.verbSArrayList.add(newVerbS);
    }
    public String getVerbS(int position) {
        return (String)this.verbSArrayList.get(position);
    }

    //Place
    public void setPlace(String newPlace) {
        this.placeArrayList.add(newPlace);
    }
    public String getPlace(int position) {
        return (String)this.placeArrayList.get(position);
    }
}
