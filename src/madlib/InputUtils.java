///////////////////////////////////////////////////////////
//InputUtils.java
// This class takes the story object retrieved from the
// MySQL database and generates a dynamic form based on the
// specific needs of that story/MadLib.
// It returns the form in a string of HTML.
///////////////////////////////////////////////////////////
package madlib;

public class InputUtils {


    public static String generateForm(Story story) {


        // Create the first part of the form.
        String htmlOutput = "<h3>Please fill in the blanks</h3><br><form action=\"MadLibServlet\" method=\"post\"><table style=\"width:20%\">";

        // create the correct number of input fields dynamically.
        //Nouns
        for (int i = 1; i < story.getTotalNoun() + 1; i++ ) {
            htmlOutput += "<tr><td>Noun " + i + ": </td><td><input name=\"noun" + i +"\" type=\"text\" /></td></tr>";
        }

        //Plural Nouns
        for (int i = 1; i < story.getTotalPNoun() + 1; i++ ) {
            htmlOutput += "<tr><td>Plural Noun " + i + ": </td><td><input name=\"pnoun" + i +"\" type=\"text\" /></td></tr>";
        }

        //Adjectives
        for (int i = 1; i < story.getTotalAdjective() + 1; i++ ) {
            htmlOutput += "<tr><td>Adjective " + i + ": </td><td><input name=\"adjective" + i +"\" type=\"text\" /></td></tr>";
        }

        //Verbs
        for (int i = 1; i < story.getTotalVerb() + 1; i++ ) {
            htmlOutput += "<tr><td>Verb " + i + ": </td><td><input name=\"verb" + i +"\" type=\"text\" /></td></tr>";
        }

        //Verb ending in ING
        for (int i = 1; i < story.getTotalVerbIng() + 1; i++ ) {
            htmlOutput += "<tr><td>Verb ending in ING " + i + ": </td><td><input name=\"verbing" + i +"\" type=\"text\" /></td></tr>";
        }

        //Part of the Body
        for (int i = 1; i < story.getTotalBPart() + 1; i++ ) {
            htmlOutput += "<tr><td>Part of the Body " + i + ": </td><td><input name=\"bpart" + i +"\" type=\"text\" /></td></tr>";
        }

        //Number
        for (int i = 1; i < story.getTotalNumber() + 1; i++ ) {
            htmlOutput += "<tr><td>Number " + i + ": </td><td><input name=\"number" + i +"\" type=\"number\" /></td></tr>";
        }

        //Past Tense Verb
        for (int i = 1; i < story.getTotalPtVerb() + 1; i++ ) {
            htmlOutput += "<tr><td>Past Tense Verb " + i + ": </td><td><input name=\"ptverb" + i +"\" type=\"text\" /></td></tr>";
        }

        //Verb ending in S
        for (int i = 1; i < story.getTotalVerbS() + 1; i++ ) {
            htmlOutput += "<tr><td>Verb Ending in S " + i + ": </td><td><input name=\"verbs" + i +"\" type=\"text\" /></td></tr>";
        }

        //Place
        for (int i = 1; i < story.getTotalPlace() + 1; i++ ) {
            htmlOutput += "<tr><td>Place " + i + ": </td><td><input name=\"place" + i +"\" type=\"text\" /></td></tr>";
        }

        // Complete the HTML form and return all the HTML in a single string.
        htmlOutput += "</table><br><p><input type=\"submit\" value=\"Submit\" /></p></form>";

        return htmlOutput;

    }
}
