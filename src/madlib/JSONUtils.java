///////////////////////////////////////////////////////////
//JSONUtils.java
// This class is heavily based on the JSON material code
// That was provided in the instructional portion of this
// course. It is used to convert the JSON stored in the
// MySQL database and turn it into an object.
///////////////////////////////////////////////////////////
package madlib;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

public class JSONUtils {

    // this is primarily used in the Setup class in order to easily setup the MySQL Database.
    public static String storyToJSON(Story story) {

        ObjectMapper mapper = new ObjectMapper();
        String storyString = "";

        try {
            storyString = mapper.writeValueAsString(story);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return storyString;
    }

    // Used every time a call is made with Hibernate to the MySQL database. It converts the JSON string to a story
    // object.
    public static Story JSONToStory(String storyString) {

        ObjectMapper mapper = new ObjectMapper();
        Story story = null;

        try {
            story = mapper.readValue(storyString, Story.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return story;
    }

}
