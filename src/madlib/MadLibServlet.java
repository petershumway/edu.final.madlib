///////////////////////////////////////////////////////////
//MadLibServlet.java
// This servlet is called directly from submitting the form on the
// first servlet. It saves the input from the previous form into the
// InputList.java class. It manages all the information needed to
// bring together the story with the user input words included.
///////////////////////////////////////////////////////////
package madlib;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "MadLibServlet", urlPatterns ={"/MadLibServlet"})
public class MadLibServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // print the output in HTML format
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");

        // get the story object again without having to query the DB again.
        StorySaver savedStory = new StorySaver();
        Story story = savedStory.getStory();

        InputList inputList = new InputList();
        StoryMapper storyMap = new StoryMapper();

        // a string to compile the full story
        String fullStory = "";

        // set the output format and open divs.
        out.println("<div style=\"float: left; position: relative; left: 50%;\"><div style=\"width:360px; float: left; position: relative; left: -50%;\">");

        // Select one of the 4 story titles based on previous the previous selection.
        switch (story.getStoryID()) {
            case 1:
                out.println("<h2>A Video Game Story</h2>");
                break;

            case 2:
                out.println("<h2>A Prom Story</h2>");
                break;

            case 3:
                out.println("<h2>An Alice's World Story</h2>");
                break;

            case 4:
                out.println("<h2>An Ireland Story</h2>");
                break;

            default:
                // error for any other invalid input
                out.println("<p> Error: Something went wrong...</p>");
                break;
        }

        // add the user input into the input lists of their proper type.
        // 1 = Noun
        for (int i = 1; i < story.getTotalNoun() + 1; i++ ) {
            inputList.setNoun(request.getParameter("noun" + i));
        }

        // 2 = Plural Noun
        for (int i = 1; i < story.getTotalPNoun() + 1; i++ ) {
            inputList.setPNoun(request.getParameter("pnoun" + i));
        }

        // 3 = Adjective
        for (int i = 1; i < story.getTotalAdjective() + 1; i++ ) {
            inputList.setAdjective(request.getParameter("adjective" + i));
        }

        // 4 = Verb
        for (int i = 1; i < story.getTotalVerb() + 1; i++ ) {
            inputList.setVerb(request.getParameter("verb" + i));
        }

        // 5 = Body Part
        for (int i = 1; i < story.getTotalBPart() + 1; i++ ) {
            inputList.setBPart(request.getParameter("bpart" + i));
        }

        // 6 = Number
        for (int i = 1; i < story.getTotalNumber() + 1; i++ ) {
            inputList.setNumber(request.getParameter("number" + i));
        }

        // 7 = Verb ending in ING
        for (int i = 1; i < story.getTotalVerbIng() + 1; i++ ) {
            inputList.setVerbIng(request.getParameter("verbing" + i));
        }

        // 8 = Past Tense Verb
        for (int i = 1; i < story.getTotalPtVerb() + 1; i++ ) {
            inputList.setPtVerb(request.getParameter("ptverb" + i));
        }

        // 9 = Verb ending in S
        for (int i = 1; i < story.getTotalVerbS() + 1; i++ ) {
            inputList.setVerbS(request.getParameter("verbs" + i));
        }

        // 10 = Place
        for (int i = 1; i < story.getTotalPlace() + 1; i++ ) {
            inputList.setPlace(request.getParameter("place" + i));
        }

        // call the story mapper to compile everything together in the proper order.
        fullStory = storyMap.getFullStory(story, inputList);

        // print the completed story/MadLib to the screen.
        out.println("<p>" + fullStory + "</p><br><p>Thanks for trying this out. <a href=\"landing.html\">Return to the beginning here!</a>.</p><br>" +
                "<p>These stories are from http://www.redkid.net/madlibs/</p></div></div>");

        // ending HTML
        out.println("</body><html>");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //redirect to the landing page in case someone gets here manually.
        response.sendRedirect("landing.html");
    }
}
