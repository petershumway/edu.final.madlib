///////////////////////////////////////////////////////////
//MySQLConnection.java
// This is a class to manage the queries and additions to the
// database. It primarily utilizes the HibernateUtils class to do this.
///////////////////////////////////////////////////////////
package madlib;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;

public class MySQLConnection {

    // Data member variables for the session, session factory & unique instance of the class. This
    // makes sure that there is only one instance of the class and that it is thread safe through the
    // session factory (in the HibernateUtils.java class).
    SessionFactory factory = null;
    Session session = null;

    private static MySQLConnection single_instance = null;

    // gets the session factory when the instance is created.
    private MySQLConnection()
    {
        factory = HibernateUtils.getSessionFactory();
    }

    // checks to make sure there is no instance yet and creates a new one. if there is an instance then it returns the
    // existing instance.
    public static MySQLConnection getInstance()
    {
        if (single_instance == null) {
            single_instance = new MySQLConnection();
        }

        return single_instance;
    }

    // this method opens a session and connects to the database. it sends a SQL query to get the data from the database.
    // it then stores the data in the LIST container, saves the interaction, and returns the LIST.
    public List<Connector> getStories() {

        // try to make sure it works correctly for error checking
        try {
            // open a new session
            session = factory.openSession();
            session.getTransaction().begin();
            // submit SQL query
            String sql = "from madlib.Connector";
            List<Connector> stories = (List<Connector>)session.createQuery(sql).getResultList();
            // commit changes
            session.getTransaction().commit();
            // return the list
            return stories;
        }
        //Catch any exceptions, show an error message, rollback any changes, return nothing.
        catch (Exception e) {
            System.out.println("There was an error retrieving the data from the database.");
            // Rollback any changes so they are not saved.
            session.getTransaction().rollback();
            return null;
        }
        // always close the session to avoid any issues.
        finally {
            session.close();
        }
    }

    // this is a method I added to add a new story line to the database and is used by the Setup.java class.
    public void addStory(int newStoryID, Story story) {

        String newJSONStory = JSONUtils.storyToJSON(story);

        // try to make sure it works correctly for error checking
        try {
            // open a new session
            session = factory.openSession();
            session.getTransaction().begin();

            // create the new game object
            Connector newConnector = new Connector();
            newConnector.setStoryID(newStoryID);
            newConnector.setJSONStory(newJSONStory);


            // save the new object
            session.save(newConnector);
            // commit the change to the database
            session.getTransaction().commit();
        }
        catch (Exception e) {
            System.out.println("There was an error adding the story information to the database.");
            // Rollback any changes so they are not saved.
            session.getTransaction().rollback();
        }
        // always close the session
        finally {
            session.close();
        }
    }
}