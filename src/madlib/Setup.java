///////////////////////////////////////////////////////////
//Setup.java
// This class is used only to add the story objects to the
// database in the correct format. It creates the story objects
// and gets everything we need in the database in the right spot.
// the program is intended to run without this class.
///////////////////////////////////////////////////////////
package madlib;

import java.util.*;

////////////////////////////////////
// number assignment for orderArray
// 1 = Noun
// 2 = Plural Noun
// 3 = Adjective
// 4 = Verb
// 5 = Body Part
// 6 = Number
// 7 = Verb ending in ING
// 8 = Past Tense Verb
// 9 = Verb ending in S
// 10 = Place
////////////////////////////////////

public class Setup {

    public static void main(String[] args) {

        ArrayList storyOA1 = new ArrayList();
        ArrayList textA1 = new ArrayList();

        storyOA1.add(4);  //Verb
        storyOA1.add(1);  //Noun
        storyOA1.add(1);  //Noun
        storyOA1.add(7);  //Verb ending in ING
        storyOA1.add(1);  //Noun
        storyOA1.add(3);  //Adjective
        storyOA1.add(5);  //Body Part
        storyOA1.add(2);  //Plural Noun
        storyOA1.add(3);  //Adjective
        storyOA1.add(5);  //Body Part
        storyOA1.add(2);  //Plural Noun
        storyOA1.add(3);  //Adjective
        storyOA1.add(2);  //Plural Noun
        storyOA1.add(3);  //Adjective
        storyOA1.add(6);  //Number
        storyOA1.add(1);  //Noun
        storyOA1.add(2);  //Plural Noun

        textA1.add("I love to ");
        textA1.add(" video games. I can play them day and ");
        textA1.add("! My mom and ");
        textA1.add(" are not too happy with my ");
        textA1.add(" so much time in front of the television ");
        textA1.add(". Although Dad believes that these ");
        textA1.add(" games help children develop hand-");
        textA1.add(" coordination and improve their learning ");
        textA1.add(", he also seems to think they have ");
        textA1.add(" side effects on one's ");
        textA1.add(". Both of my ");
        textA1.add(" think this is due to a ");
        textA1.add(" use of violence in the majority of the ");
        textA1.add(". Finally, we all arrived at a ");
        textA1.add(" compromise: After dinner I can play ");
        textA1.add(" hours of video games, provided I help clear the ");
        textA1.add(" and wash the ");
        textA1.add(".");

        Story story1 = new Story(1, 4, 4, 4, 1,
                2, 1, 1, 0, 0, 0, storyOA1, textA1);


        ////////////////////////////////////
        ArrayList storyOA2 = new ArrayList();
        ArrayList textA2 = new ArrayList();

        storyOA2.add(5);  //Body Part
        storyOA2.add(5);  //Body Part
        storyOA2.add(3);  //Adjective
        storyOA2.add(1);  //Noun
        storyOA2.add(1);  //Noun
        storyOA2.add(8);  //Past Tense Verb
        storyOA2.add(2);  //Plural Noun
        storyOA2.add(1);  //Noun
        storyOA2.add(1);  //Noun
        storyOA2.add(2);  //Plural Noun
        storyOA2.add(3);  //Adjective
        storyOA2.add(7);  //Verb ending in ING
        storyOA2.add(3);  //Adjective
        storyOA2.add(1);  //Noun
        storyOA2.add(1);  //Noun
        storyOA2.add(1);  //Noun

        textA2.add("If there's a melody you can't seem to get out of your ");
        textA2.add(" or a song running through your ");
        textA2.add(", then bring your feet to this year's ");
        textA2.add(" prom. As usual, our ");
        textA2.add(" will be held in our high school ");
        textA2.add(". A dress code will be observed. No one will be admitted wearing ");
        textA2.add(" or torn ");
        textA2.add(". Girls must wear a ");
        textA2.add(" and boys must wear a dress shirt and a ");
        textA2.add(". As always, hot ");
        textA2.add(" will be served, and there will be ");
        textA2.add(" prizes and an award for the best-");
        textA2.add(" couple. The ");
        textA2.add(" dance committee is also proud to announce that every girl who attends will receive a ");
        textA2.add(" to pin to her ");
        textA2.add(", and every boy will receive a complimentary ");
        textA2.add(".");

        Story story2 = new Story(2, 7, 2, 3, 0,
                2, 0, 1, 1, 0, 0, storyOA2, textA2);

        ////////////////////////////////////
        ArrayList storyOA3 = new ArrayList();
        ArrayList textA3 = new ArrayList();

        storyOA3.add(3);  //Adjective
        storyOA3.add(1);  //Noun
        storyOA3.add(2);  //Plural Noun
        storyOA3.add(6);  //Number
        storyOA3.add(3);  //Adjective
        storyOA3.add(9);  //Verb ending in S
        storyOA3.add(3);  //Adjective
        storyOA3.add(1);  //Noun
        storyOA3.add(1);  //Noun
        storyOA3.add(1);  //Noun
        storyOA3.add(1);  //Noun
        storyOA3.add(9);  //Verb ending in S
        storyOA3.add(1);  //Noun
        storyOA3.add(3);  //Adjective
        storyOA3.add(1);  //Noun
        storyOA3.add(2);  //Plural Noun
        storyOA3.add(3);  //Adjective
        storyOA3.add(1);  //Noun

        textA3.add("Lewis Caroll's classic, Alice's Adventures in Wonderland, as well as its ");
        textA3.add(", Through the Looking ");
        textA3.add(", have enchanted both the young and the old ");
        textA3.add(" for the last ");
        textA3.add(" years. Alice's ");
        textA3.add(" adventures begin when she ");
        textA3.add(" down a ");
        textA3.add(" hole and lands in a strange and topsy-turvy ");
        textA3.add(". There she discovers she can become a tall ");
        textA3.add(" or a small ");
        textA3.add(" simply by nibbling on alternate sides of a magic ");
        textA3.add(". In her travels through Wonderland, Alice ");
        textA3.add(" such remarkable characters as the White ");
        textA3.add(", the ");
        textA3.add(" Hatter, the Cheshire ");
        textA3.add(", and even the Queen of ");
        textA3.add(". Unfortunately, Alice's adventure come to a ");
        textA3.add(" end when Alice awakens from her ");
        textA3.add(".");

        Story story3 = new Story(3, 8, 2, 5, 0,
                0, 1, 0, 0, 2, 0, storyOA3, textA3);

        ////////////////////////////////////
        ArrayList storyOA4 = new ArrayList();
        ArrayList textA4 = new ArrayList();

        storyOA4.add(10); //Place
        storyOA4.add(2);  //Plural Noun
        storyOA4.add(3);  //Adjective
        storyOA4.add(2);  //Plural Noun
        storyOA4.add(1);  //Noun
        storyOA4.add(2);  //Plural Noun
        storyOA4.add(2);  //Plural Noun
        storyOA4.add(1);  //Noun
        storyOA4.add(1);  //Noun
        storyOA4.add(10); //Place
        storyOA4.add(2);  //Plural Noun
        storyOA4.add(2);  //Plural Noun

        textA4.add("Ireland is a beautiful green island lying directly west of ");
        textA4.add(". In 250 B.C., Ireland was inhabited by short, dark ");
        textA4.add(" who were later called \"Picts.\" They intermarried with ");
        textA4.add(" Vikings and with Celts who were ");
        textA4.add(" from Northern Europe. In 1846, a blight ruined the ");
        textA4.add(" crop in Ireland, and over a million Irishmen migrated to the United States. Many of their descendants have become very important American ");
        textA4.add(". The Irish are noted for their poetry and songs. Some of these Irish songs are: \"When Irish ");
        textA4.add(" are Smiling.\" \"Did Your ");
        textA4.add(" Come from Ireland?,\" and \"McNamara's ");
        textA4.add(".\" Thousands of American tourists go to Ireland every year to visit its capital, ");
        textA4.add(", and buy Irish linen ");
        textA4.add(" and see the beautiful ");
        textA4.add(" and lakes.");

        Story story4 = new Story(4, 3, 6, 1, 0,
                0,  0, 0, 0, 0, 2, storyOA4, textA4);


        MySQLConnection newConnection = MySQLConnection.getInstance();
        newConnection.addStory(story1.getStoryID(), story1);
        newConnection.addStory(story2.getStoryID(), story2);
        newConnection.addStory(story3.getStoryID(), story3);
        newConnection.addStory(story4.getStoryID(), story4);












    }
}
