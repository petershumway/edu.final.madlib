///////////////////////////////////////////////////////////
//Story.java
// This class stores all the needed information about each of
// the story objects.
///////////////////////////////////////////////////////////
package madlib;

import java.util.*;

public class Story {

    // Data Member Variables
    private int storyID;
    private int totalNoun;
    private int totalPNoun;
    private int totalAdjective;
    private int totalVerb;
    private int totalVerbIng;
    private int totalBPart;
    private int totalNumber;
    private int totalPtVerb;
    private int totalVerbS;
    private int totalPlace;
    private ArrayList<String> orderArray;
    private ArrayList<String> storyTextArray;

    // Empty constructor
    Story() {
        this.storyID = 0;
        this.totalNoun = 0;
        this.totalPNoun = 0;
        this.totalAdjective = 0;
        this.totalVerb = 0;
        this.totalVerbIng = 0;
        this.totalBPart = 0;
        this.totalNumber = 0;
        this.totalPtVerb = 0;
        this.totalVerbS = 0;
        this.totalPlace = 0;
        this.orderArray = new ArrayList();
        this.storyTextArray = new ArrayList();
    }

    // Full Constructor
    Story(int storyID, int totalNoun, int totalPNoun, int totalAdjective, int totalVerb, int totalBPart, int totalNumber,
          int totalVerbIng, int totalPtVerb, int totalVerbS, int totalPlace, ArrayList<String> orderArray, ArrayList<String> storyTextArray) {
        this.storyID = storyID;
        this.totalNoun = totalNoun;
        this.totalPNoun = totalPNoun;
        this.totalAdjective = totalAdjective;
        this.totalVerb = totalVerb;
        this.totalBPart = totalBPart;
        this.totalNumber = totalNumber;
        this.totalVerbIng = totalVerbIng;
        this.totalPtVerb = totalPtVerb;
        this.totalVerbS = totalVerbS;
        this.totalPlace = totalPlace;
        this.orderArray = orderArray;
        this.storyTextArray = storyTextArray;
    }

    // Getters and Setters

    // Story ID
    public void setStoryID(int newStoryID) { this.storyID = newStoryID; }
    public int getStoryID() {
        return this.storyID;
    }

    //Noun
    public void setTotalNoun(int newTotalNoun) { this.totalNoun = newTotalNoun; }
    public int getTotalNoun() {
        return this.totalNoun;
    }

    // Plural Noun
    public void setTotalPNoun(int newTotalPNoun) { this.totalPNoun = newTotalPNoun; }
    public int getTotalPNoun() {
        return this.totalPNoun;
    }

    //Adjective
    public void setTotalAdjective(int newTotalAdjective) { this.totalAdjective = newTotalAdjective; }
    public int getTotalAdjective() {
        return this.totalAdjective;
    }

    //Verb
    public void setTotalVerb(int newTotalVerb) { this.totalVerb = newTotalVerb; }
    public int getTotalVerb() {
        return this.totalVerb;
    }

    //Verb ending in ING
    public void setTotalVerbIng(int newTotalVerbIng) { this.totalVerbIng = newTotalVerbIng; }
    public int getTotalVerbIng() {
        return this.totalVerbIng;
    }

    //Body Part
    public void setTotalBPart(int newTotalBPart) {
        this.totalBPart = newTotalBPart;
    }
    public int getTotalBPart() {
        return this.totalBPart;
    }

    //Number
    public void setTotalNumber(int newTotalNumber) {
        this.totalNumber = newTotalNumber;
    }
    public int getTotalNumber() {
        return this.totalNumber;
    }

    //Past Tense Verb
    public void setTotalPtVerb(int newTotalPtVerb) {
        this.totalPtVerb = newTotalPtVerb;
    }
    public int getTotalPtVerb() {
        return this.totalPtVerb;
    }

    //Verb ending in S
    public void setTotalVerbS(int newTotalVerbs) {
        this.totalVerbS = newTotalVerbs;
    }
    public int getTotalVerbS() {
        return this.totalVerbS;
    }

    //Place
    public void setTotalPlace(int newTotalPlace) {
        this.totalPlace = newTotalPlace;
    }
    public int getTotalPlace() {
        return this.totalPlace;
    }

    //Order
    public void setOrderArray(ArrayList<String>newOrder) { this.orderArray = newOrder; }
    public ArrayList<String> getOrderArray() { return this.orderArray; }

    //Text
    public void setStoryTextArray(ArrayList<String> newStoryTextArray) { this.storyTextArray = newStoryTextArray; }
    public ArrayList<String> getStoryTextArray() { return this.storyTextArray; }

}
