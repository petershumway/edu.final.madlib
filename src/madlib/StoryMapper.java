///////////////////////////////////////////////////////////
//StoryMapper.java
// This class takes the story order and gets all the right words
// into the correct order then fills in the blanks in the story with
// the correct words.
///////////////////////////////////////////////////////////
package madlib;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class StoryMapper {

    // Empty Constructor
    StoryMapper() {

    }

    // Full Constructor
    public String getFullStory(Story story, InputList inputList) {
        // start the story as blank
        String fullStory = "";
        // a map to order everything correctly.
        Map storyOrderMap = new HashMap();
        // counters start at zero and increment to keep track of the total number of the word type.
        int nounCount = 0;
        int pNounCount = 0;
        int adjectiveCount = 0;
        int verbCount = 0;
        int verbINGCount = 0;
        int bPartCount = 0;
        int numberCount = 0;
        int pTVerbCount = 0;
        int verbSCount = 0;
        int placeCount = 0;

        ArrayList<String> order = story.getOrderArray();
        ArrayList<String> text = story.getStoryTextArray();

        // loop through the story order and put the next user input in the correct spot.
        for (int i = 0; i < order.size(); i++) {

            switch (order.get(i)) {
                case "1": //Noun
                    storyOrderMap.put(i+1, inputList.getNoun(nounCount));
                    nounCount++;
                    break;
                case "2": //Plural Noun
                    storyOrderMap.put(i+1, inputList.getPNoun(pNounCount));
                    pNounCount++;
                    break;
                case "3": //Adjective
                    storyOrderMap.put(i+1, inputList.getAdjective(adjectiveCount));
                    adjectiveCount++;
                    break;
                case "4": //Verb
                    storyOrderMap.put(i+1, inputList.getVerb(verbCount));
                    verbCount++;
                    break;
                case "5": //Body Part
                    storyOrderMap.put(i+1, inputList.getBPart(bPartCount));
                    bPartCount++;
                    break;
                case "6": //Number
                    storyOrderMap.put(i+1, inputList.getNumber(numberCount));
                    numberCount++;
                    break;
                case "7": //Verb ending in ING
                    storyOrderMap.put(i+1, inputList.getVerbIng(verbINGCount));
                    verbINGCount++;
                    break;

                case "8": //Verb ending in ING
                    storyOrderMap.put(i+1, inputList.getPtVerb(pTVerbCount));
                    pTVerbCount++;
                    break;

                case "9": //Verb ending in ING
                    storyOrderMap.put(i+1, inputList.getVerbS(verbSCount));
                    verbSCount++;
                    break;

                case "10": //Verb ending in ING
                    storyOrderMap.put(i+1, inputList.getPlace(placeCount));
                    placeCount++;
                    break;

                default:
                    // error for any other invalid input
                    fullStory = "Error: Something went wrong...";
                    return fullStory;

            }

        }

        // loop through each part of the text and then the next user input word to put the story together.
        for (int i = 0; i < text.size(); i++) {
            fullStory += text.get(i);
            if (i < order.size()) {
                fullStory += (String)storyOrderMap.get(i + 1);
            }
        }

        //return fullStory;
        return fullStory;
    }

}
