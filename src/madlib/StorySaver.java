///////////////////////////////////////////////////////////
//StorySaver.java
// A simple class to allow the story to be saved and used by
// other classes and servlets without having to query the
// database again.
///////////////////////////////////////////////////////////
package madlib;

public class StorySaver {

    private static Story savedStory;

    // Empty Constructor
    StorySaver() {

    }

    // Full constructor
    StorySaver(Story story) {
        this.savedStory = story;
    }

    // get the saved story.
    public Story getStory() {
        return this.savedStory;
    }
}
