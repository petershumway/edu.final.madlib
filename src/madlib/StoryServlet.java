///////////////////////////////////////////////////////////
//StoryServlet.java
// This is the first servlet and it is called from the initial
// for dropdown selection of a particular story. This will
// perform the one and only query to the DB for the story details.
///////////////////////////////////////////////////////////
package madlib;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

@WebServlet(name = "StoryServlet", urlPatterns ={"/StoryServlet"})
public class StoryServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String storySelection = request.getParameter("story");

        // print details in HTML format.
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");

        // collect the story number to get the right story from the query.
        int storyNum = 0;
        Story retrievedStory = new Story();

        switch (storySelection) {
            case "story1":
                storyNum = 1;
                break;

            case "story2":
                storyNum = 2;
                break;

            case "story3":
                storyNum = 3;
                break;

            case "story4":
                storyNum = 4;
                break;

            default:
                // error for any other invalid input
                out.println("<p> Error: Invalid story selection. <a href=\"landing.html\">Return to Pete's MadLibs here</a>.</p>");
                break;
        }
        // start hibernate connection
        try {
            MySQLConnection mySQLConnection = MySQLConnection.getInstance();

            List<Connector> newConnector = mySQLConnection.getStories();

            for (Connector getStory : newConnector) {
                if (getStory.getStoryID() == storyNum) {
                    retrievedStory = JSONUtils.JSONToStory(getStory.getJSONStory());
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

        // save a copy of the story retrieved from the database so we can use it in another servlet.
        StorySaver saveStory = new StorySaver(retrievedStory);

        // call method that generates a form based on the story and goes to next servlet MadLibServlet when submitted.
        out.println(InputUtils.generateForm(retrievedStory));

        // close out the HTML.
        out.println("</body><html>");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //redirect to the landing page in case someone gets here manually.
        response.sendRedirect("landing.html");

    }
}
